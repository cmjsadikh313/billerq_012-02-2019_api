@inject('request', 'Illuminate\Http\Request')



 <!-- Styles -->
        <style>
            html, body {
                background-color: #fff;
                color: #636b6f;
           
                font-weight: 100;
                height: 100vh;
                margin: 0;
                background-color: #f04f46;
                background-image: url("{{ asset('/images/bg.png') }}");
                background-size:cover ;  
            }

            .login-page, .register-page{ background: none }

            .full-height {
                height: 100vh;
            }

            .flex-center {
                align-items: center;
                display: flex;
                justify-content: center;
            }

            .position-ref {
                position: relative;
            }

            .top-right {
                position: absolute;
                right: 10px;
                top: 18px;
            }

            .content {
                text-align: center;
            }

            .title {
                font-size: 84px;
            }

            .links > a {
                color: #fff;
                padding: 0 25px;
                font-size: 12px;
                font-weight: 600;
                letter-spacing: .1rem;
                text-decoration: none;
                text-transform: uppercase;
            }

            .m-b-md {
                margin-bottom: 30px;
            }

            .tagline{
                font-size:25px;
                font-weight: 300;
            }
            a, .links > a{ color: #fff ; display: inline-block; padding:10px 15px; font-size: 50px; }
             .links > a:hover,a:hover { background: #000;   }

        </style>

<div class="container-fluid">
 

	<div class="row">
	  
<div class="col-md-12">

	     <div  class="text-center" style="max-width:600px; margin:auto; text-align: center; margin-bottom: 50px; margin-top: 100px; ">
                        <img src="{{ asset('/images/logo-large.png') }}" style="width:100%; max-width: 80% "  title="{{ config('app.name', 'ultimatePOS') }}" >
                        
               
                    </div>
</div>




        <div class="header-right-div">
         

	        @if(!($request->segment(1) == 'business' && $request->segment(2) == 'register') && $request->segment(1) != 'login')
	        	{{ __('business.already_registered')}} <a href="{{ action('Auth\LoginController@login') }}@if(!empty(request()->lang)){{'?lang=' . request()->lang}} @endif">{{ __('business.sign_in') }}</a>
	        @endif
        </div>
	</div>
</div>